# How to use: 2019_08_03_FirstGuardianPointPosition   
   
Add the following line to the [UnityRoot]/Packages/manifest.json    
``` json     
"be.eloiexperiments.firstguardianpointposition":"",    
```    
--------------------------------------    
   
Feel free to support my work: http://patreon.com/eloistree   
Contact me if you need assistance: http://eloistree.page.link/discord   
   
--------------------------------------    
``` json     
{                                                                                
  "name": "be.eloiexperiments.firstguardianpointposition",                              
  "displayName": "First Guardian Point Position",                        
  "version": "0.0.1",                         
  "unity": "2018.1",                             
  "description": "Tool that based on Guardian ID allow you to store prefab and webcam image at a relative position of the first guardian point.",                         
  "keywords": ["Script","Tool","Guardian","Position"],                       
  "category": "Script",                   
  "dependencies":{"be.eloiexperiments.oculusguardianidentity": "0.0.1"}     
  }                                                                                
```    